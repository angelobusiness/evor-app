# Evor-app Front-end

## Development Setup

> Before development setup, you need to check first if you installed `nvm` (check [Check if NVM is installed](#markdown-header-check-if-nvm-is-installed)). If `nvm` is not found in your machine, install nodejs first (check [Installing Node.js on Linux](#markdown-header-installing-nodejs-on-linux)).

Download this project using `git clone`, `cd` to the project folder, install packages, run the server, and view the app on your browser:

```bash
git clone <REPLACE_THIS_TO_THIS_PROJECT_URL>
cd evor-app
npm install
npm start
# There's a link you can click in the terminal to view the app in a browser.
# Ctrl + c in your terminal to stop the server
```

When you go back next time in your project to run it again:

```bash
npm start
# Ctrl + c in your terminal to stop the server
```

---

## Check if NVM is installed

Open a bash(linux) terminal, run:

```bash
command -v nvm
```

which should output "nvm" if the installation was successful.

---

## Installing Node.js on Linux

Upodate Linux first:

```bash
sudo apt-get update
sudo apt-get upgrade
```

We also need to install some basic build tool for [node-gyp](https://github.com/nodejs/node-gyp) - node binaries build tool.

```bash
sudo apt-get install build-essential
```

[NVM](https://github.com/creationix/nvm) tool gives you the ability to install and use multiple version of node, and prevent the (bad) usage of `sudo` for running node application, installation is via the command line:

```bash
# This command install version 0.35.0, check the NVM reference it there are newer versions. 
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh | bash
```

Restart your terminal after install.

Now you can install the latest the latest LTS version of node:

```bash
nvm install --lts
```

To update node just run the command again.

One thing you'll have to remember when use NVM is that you'll need to explicitly specify the node version you want to use (installing does it automatically on the end of the install), so next time you'll login to ubuntu you'll need to run the command:

```bash
nvm use --lts
```

You can also add this command to the end of your `.bash_profile` file:

```bash
echo "nvm use --lts" >> ~/.bash_profile
```

---

## Deploy the latest update from the repository

SSH to the remote server. CD to the correct directory. Download the updates from the repository. Install the additional npm packages.
Build the app. If NGINX is already running, then this front-end deployment is completed.

```bash
# SSH to the remote server
ssh -i YOUR_PEM.pem ubuntu@YOUR_REMOTE_SERVER.com
# Go to the app directory
cd evor-app
# download the updates from the repo
git pull
# Install the additional packages
npm install
# Build the app
npm run build
```

---

## Setup NGINX

If not existing, install nginx. Then Edit the file `default`.

```bash
# Install if not yet
command -v nginx || sudo apt install -y nginx
# Copy the default file to serve as backup
sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default-copy
# Delete the file
sudo rm /etc/nginx/sites-available/default
# Just check to verify the files that exist
ls -la /etc/nginx/sites-available/
# Edit the NGINX file config
sudo nano /etc/nginx/sites-available/default
```

Put the contents below to our `default` file in sites-available/.

```nginx
server {
  listen 80 default_server;
  server_name _;

  # react app & front-end files
  location / {
    root /home/ubuntu/evor-app/build;
    try_files $uri /index.html;
  }

  # node api reverse proxy
  location /api/ {
    proxy_pass http://localhost:4000/;
  }
}
```

Restart NGINX to apply the config changes

```bash
sudo systemctl restart nginx
```

Enable NGINX on server reboot

```bash
sudo systemctl enable nginx
```

Now if all works, you should see your site in the browser. If anything is wrong, you may need to modify the Firewall settings.

---

## Modify the UFC (Firewall)

```bash
# allow ssh connections through firewall
sudo ufw allow OpenSSH

# allow http & https through firewall
sudo ufw allow 'Nginx Full'

# enable firewall
sudo ufw --force enable
```
