import axios from "axios";
import authHeader from './auth-header';

import { config } from './config';
const API_URL = config.API_URL + '/api/auth/';

const saveUserData = (userData) => {
  localStorage.setItem("user", JSON.stringify(userData));
};

const updateUserData = (key, value) => {
  let userData = JSON.parse(localStorage.getItem('user'));
  if (userData) {
    userData[key] = value;
    localStorage.setItem("user", JSON.stringify(userData));
  }
};

class AuthService {
  login(email, password) {
    return axios.post(API_URL + "signin", {
        email, password
      })
      .then(response => {
        if (response.data.accessToken) {
          saveUserData(response.data);
        }
        return response;
      });
  }

  logout() {
    localStorage.removeItem("user");
  }

  register({ nextPath, ...data }) {
    return axios.post(API_URL + "signup", data)
      .then(response => {
        if (response.data.accessToken) {
          saveUserData({ nextPath, ...response.data });
        }
        return response;
      });
  }

  verifyEmailCaptcha({ nextPath, ...data }) {
    return axios.post(API_URL + "signin/verify-email-captcha", data)
      .then(response => {
        if (response.data.accessToken) {
          saveUserData({ nextPath, ...response.data });
        }
        return response;
      });
  }

  setNewPassword({ nextPath, ...data }) {
    return axios.post(API_URL + "signin/set-new-password", data, { headers: authHeader() })
      .then(response => {
        updateUserData('status', 'completed');
        return response;
      });
  }

  generateNewCode() {
    return axios.post(API_URL + "signin/generate-new-code", {}, { headers: authHeader() })
      .then(response => {
        response.data.timeout && updateUserData('timeout', response.data.timeout);
        return response;
      });
  }

  verifyCode({ nextPath, code }) {
    return axios.post(API_URL + "signup/verify-code", { code }, { headers: authHeader() })
      .then(response => {
        updateUserData('nextPath', nextPath);
        return response;
      });
  }

  setCountry({ country }) {
    return axios.post(API_URL + "signup/set-country", { country }, { headers: authHeader() })
      .then(response => {
        updateUserData('status', 'completed');
        return response;
      });
  }

  getCurrentUser() {
    return JSON.parse(localStorage.getItem('user'));
  }

  userLoggedIn() {
    let user = JSON.parse(localStorage.getItem('user'));
    return user && user.scope !== "registration";
  }

  isRegistering() {
    let user = JSON.parse(localStorage.getItem('user'));
    return user && user.scope === "registration";
  }
}

export default new AuthService();
