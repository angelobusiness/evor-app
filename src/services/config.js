const prod = {
  API_URL: 'http://ec2-34-217-120-90.us-west-2.compute.amazonaws.com',
  RECAPTCHA_SITE_KEY: '6Lf1zaQZAAAAAK3Pjxf3uxA8YiLw-i8zWbS3C8VM',
  RECAPTCHA_SECRET_KEY: '6Lf1zaQZAAAAANQITIWbX9mldhnRdiWXVQnmodmN'
};

const dev = () => {
  const loc = window.location;
  let url = loc.origin.replace(loc.port, '8080');
  return { 
    API_URL: url,
    RECAPTCHA_SITE_KEY: '6Lf1zaQZAAAAAK3Pjxf3uxA8YiLw-i8zWbS3C8VM',
    RECAPTCHA_SECRET_KEY: '6Lf1zaQZAAAAANQITIWbX9mldhnRdiWXVQnmodmN'
  };
};

export const config = process.env.REACT_APP_ENV === 'production'
  ? prod
  : dev();