import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link, Redirect, withRouter } from "react-router-dom";
import PrivateLoginRoute from "./pages/private-login-route";
import PrivateRegRoute from "./pages/private-reg-route";
import PrivateRoute from "./pages/private-route";

import SplashScenes from "./components/splash-scenes";

import LoginPage from "./pages/login-page";
import ForgotPasswordPage from "./pages/forgot-password-page";
import SetPasswordPage from "./pages/set-password-page";

import RegisterPage from "./pages/register-page";
import HomePage from "./pages/home-page";
import VerifyCodePage from "./pages/verify-code-page";
import SelectCountryPage from "./pages/select-country-page";
import NotificationPage from "./pages/notification-page";
import HistoryPage from "./pages/history-page";
import HistoryViewPage from "./pages/history-view-page";
import Nav from "./components/nav.component";
import NotFoundPage from "./pages/not-found-page";

import AuthService from "./services/auth.service";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: undefined,
      splashDidEnd: false,
    };

    this.onEndSplashHandler = this.onEndSplashHandler.bind(this);
    this.logOut = this.logOut.bind(this);
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();
    if (user) {
      this.setState({
        currentUser: user
      });
    }
  }

  onEndSplashHandler() {
    this.setState({ splashDidEnd: true });
  }

  logOut() {
    AuthService.logout();
    this.setState({
      currentUser: undefined
    });
    this.props.history.push("/");
  }

  render() {
    const { currentUser, splashDidEnd } = this.state;
    // const { location } = this.props;

    return (
      <Router>
        <Nav onLogout={this.logOut} />
        <SplashScenes onEndSplashHandler={this.onEndSplashHandler} />
        <div className={"container-fluid main"}>
          {splashDidEnd && (
            <Switch>
              <Route exact path={["/", "/home"]} component={HomePage} />
              <Route exact path="/login" component={LoginPage} />
              <Route exact path="/login/forgot-password" component={ForgotPasswordPage} />
              <PrivateLoginRoute exact path="/login/set-new-password" component={SetPasswordPage} />

              <Route exact path="/register" component={RegisterPage} />
              <PrivateRegRoute exact path="/register/verify-code" component={VerifyCodePage} />
              <PrivateRegRoute exact path="/register/select-country" component={SelectCountryPage} />
              <PrivateRoute path="/history-view/:type" component={HistoryViewPage} />
              <PrivateRoute exact path="/history" component={HistoryPage} />
              <PrivateRoute exact path="/notifications" component={NotificationPage} />
              <Route exact path="/404" component={NotFoundPage} />
              <Route component={NotFoundPage} />
            </Switch>
          )}
        </div>
      </Router>
    );
  }
}

export default withRouter(App);