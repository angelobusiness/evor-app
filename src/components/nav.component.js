import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import AuthService from "../services/auth.service";

class Nav extends Component {
  constructor(props) {
    super(props);

    this.logOut = this.logOut.bind(this);
  }

  logOut() {
    this.props.onLogout && this.props.onLogout();
  }

  render() {
    return (<>
      {AuthService.userLoggedIn() && (
      <nav className="main-nav hidden">
        <Link to="" onClick={this.logOut}> Logout </Link>
      </nav>
      )}
      </>
    );
  }
}

export default withRouter(Nav);