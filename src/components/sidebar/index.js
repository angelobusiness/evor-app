import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";

import "./styles.scss";

import AuthService from "../../services/auth.service";

const Sidebar = (props) => {
  const [loggedOut, setLoggedOut] = useState(false);
  if (loggedOut) {
    return <Redirect to="/" />;
  }

  const handleLogout = (e) => {
    e.preventDefault();
    AuthService.logout();
    setLoggedOut(true);
  };

  let classNames = "sidebar ";
  classNames += (props.isOpen) ? "sidebar-open" : "sidebar-close";

  return (
    <div className={classNames} onClick={(e) => { e.preventDefault(); (e.target === e.currentTarget) && props.onOverlayClick && props.onOverlayClick(); }}>
      <div className="sidebar-inner">
        <ul className="sidebar-ul">
          <li className="list-item">
            <a href="#" className="list-link"><i className="ico fa-user"></i>Profile</a>
          </li>
          <li className="list-item">
            <a href="#" className="list-link"><i className="ico fa-users"></i>Remittance Party</a>
          </li>
          <li className="list-item">
            <Link to="/history" className="list-link"><i className="ico fa-calendar-alt"></i>History</Link>
          </li>
        </ul>
        <ul className="sidebar-ul-bottom">
          <li className="list-item">
            <a href="#" className="list-link"><i className="ico fa-question-circle"></i>Help</a>
          </li>
          <li className="list-item">
            <a href="#" className="list-link" onClick={handleLogout}><i className="ico fa-power-off"></i>Logout</a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;