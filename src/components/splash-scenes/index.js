import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./styles.css";

class SplashScenes extends Component {
  constructor(props) {
    super(props);

    // const pathname = props.location.pathname;
    this.state = {
      /* Default scene to show up first */
      scene: 1,
      /* 
       ended = false -> means that the splash scenes will show up.
       Make the splash screen show up only when the route, on 
       window load or reload, is the homepage */
      // ended: !(pathname === "/" || pathname === "/home"),
      ended: props.forceEnd || false,
    };

    this.timeout = 0;
    this.numScenes = 2;
    this.waitTimes = [2500, 3500];
  }

  componentDidMount() {
    if (this.timeout) {
      return;
    }

    if (this.state.ended) {
      this._fireEventOnEndSplash();
    } else {
      this._loopScenes();
    }
  }

  _fireEventOnEndSplash() {
    this.props.onEndSplashHandler && this.props.onEndSplashHandler();
  }

  _loopScenes() {
    const { scene } = this.state;
    const numScenes = this.numScenes;

    this.timeout = setTimeout(() => {
      const nextScene = scene + 1;
      if (nextScene > numScenes) {
        this.setState({ ended: true });
        this._fireEventOnEndSplash();
        return;
      }
      else {
        this.setState({ scene: nextScene });
        this._loopScenes();
      }
    }, this.waitTimes[scene-1]);
  }

  render() {
    const { scene, ended } = this.state;
    const parentClasses = "splashscenes" + (ended ? " did-end" : "");

    return (
      <div className={parentClasses}>
        {(function () {
          switch (scene) {
            case 2:
              return (
                <div className={`scene scene-${scene}`}>
                  <div>
                    <h2>Welcome to</h2>
                    <h1> </h1>
                    <div className="p-parent">
                      <p className="first">The cheapest and</p>
                      <p className="second">safest global remittance!</p>
                    </div>
                  </div>
                </div>
              );
            case 1:
            default:
              return (
                <div className="scene scene-1">
                  <div className="logo-gif"></div>
                </div>
              );
          }
        })()}
      </div>
    );
  }
}

export default withRouter(SplashScenes);