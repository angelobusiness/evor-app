import React from "react";

import "./styles.scss";

const CurrencyCard = (props) => {
  const { data } = props;
  return (
    <div className="currency-card">
      <div className="currency-card-top">
        <span className="country-flag">{data.iso}</span>
        <span className="currency-name">{data['currency-name']}</span>
      </div>
      <div className="currency-card-body">
        <span className="balance">{data.balance[0]}.<span className="cents">{data.balance[1]}</span></span>
        <span className="currency-code">{data.code}</span>
      </div>
      <div className="currency-card-bottom">
        <ul className="nav justify-content-end">
          <li className="nav-item">
            <a className="currency-card-nav-link nav-link" href="#"><span className="temporary-icon"></span></a>
          </li>
          <li className="nav-item">
            <a className="currency-card-nav-link nav-link" href="#"><span className="temporary-icon"></span></a>
          </li>
          <li className="nav-item">
            <a className="currency-card-nav-link nav-link" href="#"><span className="temporary-icon"></span></a>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default CurrencyCard;