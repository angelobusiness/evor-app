import React from "react";
import { Link, withRouter } from 'react-router-dom'

import "./styles.scss";

const HistoryItem = (props) => {
  const { type, date, message } = props;

  return (
    <li className="history-item px-3 pt-3">
      <div className="d-flex pb-3">
        <div className="col px-0">
          <span className="t-type badge badge-primary">RECEIVE</span><br />
          <span className="t-email">aaa@email.com</span><br />
          <span className="currency-code">PHP</span><br />
        </div>
        <div className="col px-0 text-right">
          <span className="t-dt">date time</span><br />
          <span className="t-amt">1,234.00<span className="code">Php</span></span><br />
          <span className="t-fee">Fee: 2.50 Php</span>
        </div>
      </div>
    </li>
  );
};

const HistoryList = (props) => {
  const { rightLink } = props;

  const items = [
    {
      type: 'transfer',
      email: 'aaa@email.com',
      code: 'PHP',
      datetime: '2020-06-25 03:00:00',
      amount: '1,234.00',
      fee: '0.90',
    },
    {
      type: 'transfer',
      email: 'aaa@email.com',
      code: 'PHP',
      datetime: '2020-06-25 03:00:00',
      amount: '1,234.00',
      fee: '0.90',
    }

  ];
  const nKeys = Object.keys(items);

  let rightLinkElement;
  if (rightLink === 'view-all') {
    rightLinkElement = <Link to={`/history-view/${props.type}`} className="link-tpl-1"> View All </Link>;
  }
  else {
    rightLinkElement = <Link to="" onClick={props.history.goBack} className="link-tpl-1"> Go Back </Link>;
  }

  return (
    <div className="history-list-wrap">
      <div className="px-3">
        <div className="history-list-top d-flex">
          <div className="col col-auto px-0"><div className="hl-title">{props.type}</div></div>
          <div className="col text-right px-0">{rightLinkElement}</div>
        </div>
      </div>
      <ul className="history-list list-unstyled">
        {nKeys.map((index) => (
          <HistoryItem
            key={index}
            type={items[index].type}
            email={items[index].email}
            code={items[index].code}
            datetime={items[index].datetime}
            amount={items[index].amount}
            fee={items[index].fee}
          />
        ))}
      </ul>
    </div>
  );
};

export default withRouter(HistoryList);