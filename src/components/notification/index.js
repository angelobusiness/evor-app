import React from "react";

import "./styles.scss";

const NotificationItem = (props) => {
  const { type, date, message } = props;

  let typeElement;
  if (type === '[important]') {
    typeElement = <span className="transaction-type text-uppercase text-danger">{type}</span>;
  }
  else {
    typeElement = <span className="transaction-type">{type}</span>;
  }

  return (
    <li className="notification-item py-3">
      {typeElement}
      <span className="transaction-date">{date}</span>
      <p className="transaction-message">
        {message}
      </p>
    </li>
  );
};

const NotificationList = (props) => {
  const { notifications } = props;
  const nKeys = Object.keys(notifications);

  return (
    <ul className="notification-list">
      {nKeys.map((index) => (
        <NotificationItem
          key={index}
          type={notifications[index].type}
          date={notifications[index].date || null}
          message={notifications[index].message}
        />
      ))}
    </ul>
  );
};

export default NotificationList;