import PropTypes from 'prop-types';
import React from "react";
import { control } from 'react-validation';
import Dropdown from 'react-bootstrap/Dropdown';
import countries from './countries';
import "./styles.scss";

// BS Dropdown = Bootstrap Dropown

const countryIsos = Object.keys(countries);

const getData = ({ iso, data }) => {
  if (countries[iso] && countries[iso][data]) {
    return countries[iso][data];
  }

  if (data === 'name') {
    return 'Select Country';
  }

  return '---';
};

const Input = ({ error, isChanged, isUsed, ...props }) => {
  props.value = props.value || "";

  // Just override the property type to be always "checkbox"
  props.type = "text";
  props.className = `${props.className || ""} hidden`;
  if (isChanged && isUsed && error) {
    props.className = `is-invalid-input ${props.className}`;
  }

  return (<>
    <Dropdown className="country-dropdown-wrapper" onSelect={(iso) => { props.onSelect && props.onSelect(iso); }}>
      <Dropdown.Toggle variant="outline-primary btn-lg" id="country-dropdown"><span className={`country-iso country-iso-${props.value}`}>{getData({ iso: props.value, data: 'name' })}</span></Dropdown.Toggle>
      <Dropdown.Menu className="outline">
        {countryIsos.map((iso, index) => (
          <Dropdown.Item key={`${iso}-` + index.toString()} eventKey={iso}><div><span className={`country-iso country-iso-${iso}`}>{getData({ iso, data: 'name' })}</span><span className="country-code">{getData({ iso, data: 'code' })}</span></div></Dropdown.Item>
        ))}
      </Dropdown.Menu>
    </Dropdown>
    <input {...props} {...({ className: props.className })} />
    {isChanged && isUsed && error}
  </>);
};

Input.propTypes = {
  error: PropTypes.oneOfType([PropTypes.node, PropTypes.string])
};

export default control(Input);
