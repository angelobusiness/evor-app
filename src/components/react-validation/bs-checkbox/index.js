import React from "react";
import PropTypes from 'prop-types';
import { control } from 'react-validation';

// BS Checkbox = Bootstrap Checkbox

const Input = ({ error, isChanged, isUsed, children, ...props }) => {
  // Just override the property type to be always "checkbox"
  props.type = "checkbox";
  var parentClassName = "custom-control custom-checkbox";
  props.className = `custom-control-input ${props.className || ''}`;
  if (isChanged && isUsed && error) {
    parentClassName = `is-invalid-input-group ${parentClassName}`;
    props.className = `is-invalid-input ${props.className}`;
  }
  return (<>
    <div className={parentClassName}>
      <input {...props} />
      <label className="custom-control-label" htmlFor={props.id}>{children}</label>
    </div>
    { isChanged && isUsed && error }
  </>);
};

Input.propTypes = {
  error: PropTypes.oneOfType([PropTypes.node, PropTypes.string])
};

export default control(Input);
