import React from "react";
import PropTypes from 'prop-types';
import { control } from 'react-validation';

const Input = ({ error, isChanged, isUsed, inputRef, ...props }) => {
  return (<>
    <input {...props} {...(isChanged && isUsed && error ? {
      className: `is-invalid-input ${props.className}`
    } : { className: props.className })} ref={inputRef} />
    {isChanged && isUsed && error}
  </>)
};

Input.propTypes = {
  error: PropTypes.oneOfType([PropTypes.node, PropTypes.string])
};

export default control(Input);
