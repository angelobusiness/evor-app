import React, { Component } from "react";

export default class NotFoundPage extends Component {
  render() {
    return (
      <div className="p-404 d-flex align-items-center justify-content-center">
        <h3>404 - Not Found</h3>
      </div>
    );
  }
}
