import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import Form from "react-validation/build/form";
import CustomInput from "../components/react-validation/input";
import CheckButton from "react-validation/build/button";
import { isEmail, isAlphanumeric } from "validator";

import AuthService from "../services/auth.service";

const required = (value) => {
  if (!value) {
    return (<>
      <p className="error-msg text-danger">This field is required.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

const email = value => {
  if (!isEmail(value)) {
    return (<>
      <p className="error-msg text-danger">Invalid email.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

const vpassword = value => {
  if (value.length < 8) {
    return (<>
      <p className="error-msg text-danger">Please input at least 8 alphanumeric characters.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
  else if (value.length >= 8 && !isAlphanumeric(value)) {
    return (<>
      <p className="error-msg text-danger">Please input alphanumeric characters.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
  else if (value.length > 100) {
    return (<>
      <p className="error-msg text-danger">Length is too long.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

const vpassing = (value) => {
  if (value.length > 0) {
    // return (<ion-icon p-float="right" name="checkmark-circle"></ion-icon>);
  }
};

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePass = this.onChangePass.bind(this);
    this.onToggleShowPass = this.onToggleShowPass.bind(this);

    this.state = {
      email: "",
      password: "",
      loading: false,
      message: "",
      showpassword: false
    };
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangePass(e) {
    this.setState({
      password: e.target.value
    });
  }

  handleLogin(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    this.form.validateAll();

    console.log('login validating..  error #:', this.checkBtn.context._errors.length);
    if (this.checkBtn.context._errors.length === 0) {
      AuthService.login(this.state.email, this.state.password).then(
        () => {
          this.props.history.push("/");
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({
        loading: false
      });
    }
  }

  onToggleShowPass(target_name, e) {
    const name = `show${target_name}`;
    const value = !this.state[name];
    this.setState({ [name]: value });
    this.inputPass.focus();
  }

  componentDidMount() {
    setTimeout(() => {
      this.inputEmail && this.inputEmail.focus();
    }, 0);
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/' } };

    if (AuthService.getCurrentUser()) {
      return <Redirect to={from} />;
    }

    return (
      <div className="early-step login-page row">
        <div className="inner">
          <div className="logo-col"></div>
        </div>
        <div className="inner">
          <div className="max-width-330">
            <div className="logo-gif"></div>
            <h2>Welcome!</h2>
            <p>Sign in to access your account.</p>

            <Form
              className="mt-4 px-4"
              onSubmit={this.handleLogin}
              ref={c => { this.form = c; }}
            >
              <div className="form-group pos-relative">
                <ion-icon p-float="left" name="person-outline"></ion-icon>
                <CustomInput
                  inputRef={c => { this.inputEmail = c; }}
                  className="form-control form-control-lg form-control-tpl1"
                  type="email" name="email" placeholder="Email address"
                  value={this.state.email} onChange={this.onChangeEmail}
                  validations={[required, email, vpassing]}
                />
              </div>

              <div className="form-group pos-relative form-group-password mb-0">
                <ion-icon p-float="left" name="key"></ion-icon>
                <ion-icon p-float="right" name={this.state.showpassword ? "eye" : "eye-off"} onClick={this.onToggleShowPass.bind(this, "password")}></ion-icon>
                <CustomInput
                  inputRef={c => { this.inputPass = c; }}
                  className="form-control form-control-lg form-control-tpl1"
                  type={this.state.showpassword ? "text" : "password"} name="password" placeholder="Password"
                  value={this.state.password} onChange={this.onChangePass}
                  validations={[required, vpassword, vpassing]}
                />
              </div>
              <p className="forgot-password-wrap text-right">
                <Link to={"/login/forgot-password"} className="text-white">Forgot Password?</Link>
              </p>

              <div className="form-group">
                <button
                  className="btn btn-lg btn-outline-primary btn-block"
                  disabled={this.state.loading}
                >
                  {this.state.loading && (
                    <span className="submit-spinner spinner-border spinner-border-sm"></span>
                  )}
                  <span>LOGIN</span>
                </button>
              </div>

              {this.state.message && (
                <div className="form-group">
                  <div className="alert alert-danger" role="alert">
                    {this.state.message}
                  </div>
                </div>
              )}
              <CheckButton
                style={{ display: "none" }}
                ref={c => { this.checkBtn = c; }}
              />
            </Form>
          </div>
        </div>
      </div>
    );
  }
}
