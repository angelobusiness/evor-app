import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import StartPage from "./start-page";
import DashboardPage from "./dashboard-page";

import AuthService from "../services/auth.service";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  render() {
    const user = AuthService.getCurrentUser();

    if (user && user.scope) {
      AuthService.logout();
      return <StartPage />;
    }
    else if (!user) {
      return <StartPage />;
    }

    return <DashboardPage />;
  }
}
