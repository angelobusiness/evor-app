import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Form from "react-validation/build/form";
import CustomInput from "../components/react-validation/input";
import CheckButton from "react-validation/build/button";
import { isAlphanumeric } from "validator";
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

import AuthService from "../services/auth.service";

const required = (value, { name }) => {
  if (!value) {
    return (<>
      <p className="v-error-text text-danger">This field is required.</p>
      {name !== 'code' && (<ion-icon p-float="right" name="alert-circle"></ion-icon>)}
    </>);
  }
};

const vpassword = value => {
  if (value.length < 8) {
    return (<>
      <p className="v-error-text text-danger">Please input at least 8 alphanumeric characters.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
  else if (value.length >= 8 && !isAlphanumeric(value)) {
    return (<>
      <p className="v-error-text text-danger">Please input alphanumeric characters.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
  else if (value.length > 100) {
    return (<>
      <p className="v-error-text text-danger">Length is too long.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

const vconfirmpass = (value, props, components) => {
  if (value !== components['password'][0].value) {
    return (<>
      <p className="v-error-text text-danger">Password didn't match!</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

const getTimeoutLeft = (defaultTimeout) => {
  // get the last time the code was generated. (codeGeneratedAt)
  let dt = localStorage.getItem('codeGenAt');
  if (typeof dt == "null") {
    return null;
  }
  dt = parseInt(dt);
  if (isNaN(dt)) {
    return defaultTimeout || null;
  }
  const elapsedSec = ((new Date).getTime() - dt) / 1000;
  return Math.floor(defaultTimeout - elapsedSec);
};

export default class SetPassword extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangePass = this.onChangePass.bind(this);
    this.onChangeConfirmPass = this.onChangeConfirmPass.bind(this);
    this.onResetPasswordComplete = this.onResetPasswordComplete.bind(this);
    this.onChangeCode = this.onChangeCode.bind(this);
    this.onGetCode = this.onGetCode.bind(this);

    this.state = {
      password: "",
      confirmpass: "",
      code: "",
      loading: false,
      toLogin: false,
      successful: false,
      message: "",
      showpassword: false,
      showconfirmpass: false,
      codeLoading: false,
      codeCounter: 1, // default should be greater than zero to prevent displaying code expired
      codeRefetch: false,
      counterState: ""
    };

    this.nextPath = "/login";
    this.timeout = null;
    this.codeCounterDefault = 300;
  }

  onResetPasswordComplete() {
    localStorage.removeItem('codeGenAt');
    // Remove user data with scope=login
    AuthService.logout();
    // Redirect to login
    this.setState({ toLogin: true });
  }

  onChangePass(e) {
    this.setState({ password: e.target.value });
  }

  onChangeConfirmPass(e) {
    this.setState({ confirmpass: e.target.value });
  }

  onToggleShowPass(target_name, e) {
    const name = `show${target_name}`;
    const value = !this.state[name];
    this.setState({ [name]: value });

    if (name === 'showpassword') {
      this.inputPass.focus();
    }
    else {
      this.inputConfirmPass.focus();
    }
  }

  onChangeCode(e) {
    this.setState({ code: e.target.value });
  }

  onGetCode() {
    // Generate and resend new code
    this.setState({
      codeLoading: true,
      codeSuccessful: false,
      counterState: "stopped"
    });
    this.stopCodeCounter();

    AuthService.generateNewCode().then(
      response => {
        this.setState({
          codeLoading: false,
          codeSuccessful: true,
          codeRefetch: true
        });
        localStorage.setItem('codeGenAt', (new Date).getTime());
        this.reStartCodeCounter(response.data.timeout);
      },
      error => {
        const resMessage =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        this.setState({
          codeLoading: false,
          codeSuccessful: false,
          message: resMessage
        });
      }
    );
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      const { password, code } = this.state;
      const nextPath = this.nextPath;
      AuthService.setNewPassword({ nextPath, password, code }).then(
        () => {
          this.setState({
            loading: false,
            message: "",
            successful: true
          });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({
        loading: false
      });
    }
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();
    const timeout = getTimeoutLeft(user.timeout || this.codeCounterDefault);
    if (timeout <= 0) {
      this.state.codeCounter = 0;
      this.stopCodeCounter();
    }
    else this.reStartCodeCounter(timeout);
  }

  reStartCodeCounter(timeout) {
    this.stopCodeCounter();

    this.state.codeCounter = timeout || this.codeCounterDefault;
    const timer = () => {
      this.setState((state) => (
        { codeCounter: state.codeCounter - 1 }
      ));
      if (this.state.codeCounter <= 0) {
        this.stopCodeCounter();
      }
      // console.log(this.state.codeCounter);
    };

    this.interval = setInterval(timer, 1000);
    this.setState({ counterState: "running" });
  }

  stopCodeCounter() {
    if (this.interval) {
      clearInterval(this.interval);
    }
    this.setState({ counterState: "stopped" });
  }

  componentWillUnmount() {
    this.stopCodeCounter();
  }

  render() {
    let showModal = false;
    if (this.state.toLogin) {
      return <Redirect to={this.nextPath} />;
    }
    else if (this.state.successful) {
      showModal = true;
    }
    else {
      const user = AuthService.getCurrentUser();
      if (user) {
        if (user.scope == "login_reset_pass") {
          // user is still on (login change password) process.
          const nextPath = user.nextPath || this.nextPath;
          const { pathname } = this.props.location;
          if (pathname !== nextPath)
            return <Redirect to={nextPath} />;
        }
        else if (user.scope != "registration") {
          // user has logged in.
          const { from } = this.props.location.state || { from: { pathname: '/' } };
          return <Redirect to={from} />;
        }
      }
    }

    return (
      <div className="early-step set-password-page row">
        <div className="inner">
          <div className="logo-col"></div>
        </div>
        <div className="inner">
          <div className="max-width-330">

            {showModal ? (
              <Modal className="registration-completed-modal" show={true} animation={false} centered>
                <Modal.Body>
                  <ion-icon name="checkmark-circle" size="large"></ion-icon>
                  <p className="modal-body-head mt-2">PASSWORD CHANGE COMPLETED</p>
                  <p>
                    Thank you for using Evor! You have successfully changed your password.
                    Please login with your new password.
                  </p>
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="primary" onClick={this.onResetPasswordComplete} style={{ letterSpacing: "1.2px", minWidth: "130px" }}>LOG IN</Button>
                </Modal.Footer>
              </Modal>
            ) : (
                <Form
                  className="mt-5 px-4"
                  onSubmit={this.handleSubmit}
                  ref={c => { this.form = c; }}
                >
                  <div className="form-group pos-relative">
                    <ion-icon p-float="left" name="key"></ion-icon>
                    <ion-icon p-float="right" name={this.state.showpassword ? "eye" : "eye-off"} onClick={this.onToggleShowPass.bind(this, "password")}></ion-icon>
                    <CustomInput
                      inputRef={c => { this.inputPass = c; }}
                      className="form-control form-control-lg form-control-tpl1"
                      type={this.state.showpassword ? "text" : "password"} name="password" placeholder="Password"
                      value={this.state.password} onChange={this.onChangePass}
                      validations={[required, vpassword]}
                    />
                  </div>

                  <div className="form-group pos-relative">
                    <ion-icon p-float="left" name="key"></ion-icon>
                    <ion-icon p-float="right" name={this.state.showconfirmpass ? "eye" : "eye-off"} onClick={this.onToggleShowPass.bind(this, "confirmpass")}></ion-icon>
                    <CustomInput
                      inputRef={c => { this.inputConfirmPass = c; }}
                      className="form-control form-control-lg form-control-tpl1"
                      type={this.state.showconfirmpass ? "text" : "password"} name="confirmpass" placeholder="Confirm Password"
                      value={this.state.confirmpass} onChange={this.onChangeConfirmPass}
                      validations={[required, vconfirmpass]}
                    />
                  </div>

                  <div className="form-group pos-relative form-group-code">
                    <div className="row">
                      <div className={"col-7 pr-1" + ((!this.state.codeLoading && this.state.codeSuccessful) || !this.state.codeRefetch ? " has-extra-msg" : "")}>
                        <CustomInput
                          className="form-control form-control-lg inp-code"
                          type="number" name="code" placeholder="Verification Code"
                          value={this.state.code} onChange={this.onChangeCode}
                          validations={[required]}
                        />
                        {!this.state.codeRefetch && (<p className="small-text">Check your email!</p>)}
                        {!this.state.codeLoading && this.state.codeSuccessful && (<p className="small-text">Code has been sent!</p>)}
                      </div>
                      <div className="col-5 pl-0">
                        <Button variant="primary btn-lg btn-get-code" onClick={this.onGetCode} disabled={this.state.codeLoading || this.state.codeCounter > 99}>
                          {!this.state.codeLoading && ("Get Code")}
                          {this.state.codeLoading && (<span className="spinner-border spinner-border-sm"></span>)}
                        </Button>
                        {this.state.counterState == "running" && this.state.codeCounter < 100 && (
                          <p className="small-text text-right with-counter">Code expires in <span>{this.state.codeCounter}</span></p>
                        )}
                        {this.state.counterState == "stopped" && this.state.codeCounter <= 0 && (
                          <p className="small-text text-right text-danger">Code expired.</p>
                        )}
                      </div>
                    </div>
                  </div>

                  <div className="form-group">
                    <button
                      className="btn btn-lg btn-outline-primary btn-block"
                      disabled={this.state.loading}
                    >
                      {this.state.loading && (
                        <span className="submit-spinner spinner-border spinner-border-sm"></span>
                      )}
                      <span>NEXT</span>
                    </button>
                  </div>

                  {this.state.message && (
                    <div className="form-group">
                      <div className="alert alert-danger" role="alert">
                        {this.state.message}
                      </div>
                    </div>
                  )}
                  <CheckButton
                    style={{ display: "none" }}
                    ref={c => { this.checkBtn = c; }}
                  />
                </Form>
              )}

          </div>
        </div>
      </div>
    );
  }
}
