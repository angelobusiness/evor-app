import React, { Component } from "react";
import { Link } from 'react-router-dom'
import AuthService from "../services/auth.service";

import "./dashboard.scss";
import Nav from 'react-bootstrap/Nav';
import Sidebar from '../components/sidebar';
import HistoryList from '../components/history';

export default class HistoryPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: AuthService.getCurrentUser(),
      showSidebar: false
    };
  }

  render() {
    return (
      <div className="row">
        <div className="dashboard-tab-container dashboard-container with-nav-header with-main-nav-tabs max-w-600 px-0">
          <Sidebar isOpen={this.state.showSidebar} onOverlayClick={() => { this.setState({ showSidebar: false }); }} />
          <Nav className="nav-header max-w-600">
            <Nav.Item>
              <Nav.Link className="nav-toggle-menu" onClick={(e) => { e.preventDefault(); this.setState({ showSidebar: true }); }}>Menu</Nav.Link>
            </Nav.Item>
            <Nav.Item className="flex-fill text-center">
              <span className="nav-link nav-title">HISTORY</span>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link as={Link} className="nav-notification" to="/notifications">Noti.</Nav.Link>
            </Nav.Item>
          </Nav>
          <div className="history-container">
            <HistoryList type="transfer" rightLink="view-all"/>
            <HistoryList type="exchange" rightLink="view-all"/>
            <HistoryList type="deposit" rightLink="view-all"/>
            <HistoryList type="withdrawal" rightLink="view-all"/>
          </div>
        </div>
      </div>
    );
  }
}
