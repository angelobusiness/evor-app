import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import Form from "react-validation/build/form";
import CustomInput from "../components/react-validation/input";
import BsCheckbox from "../components/react-validation/bs-checkbox";
import CheckButton from "react-validation/build/button";
import { isEmail, isAlphanumeric } from "validator";
import AuthService from "../services/auth.service";
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

const required = (value) => {
  if (!value) {
    return (<>
      <p className="text-danger">This field is required.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

const email = value => {
  if (!isEmail(value)) {
    return (<>
      <p className="text-danger">Invalid email.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

const vpassword = value => {
  if (value.length < 8) {
    return (<>
      <p className="text-danger">Please input at least 8 alphanumeric characters.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
  else if (value.length >= 8 && !isAlphanumeric(value)) {
    return (<>
      <p className="text-danger">Please input alphanumeric characters.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
  else if (value.length > 100) {
    return (<>
      <p className="text-danger">Length is too long.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

const vconfirmpass = (value, props, components) => {
  if (value !== components['password'][0].value) {
    return (<>
      <p className="text-danger">Password didn't match!</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

const vpassing = (value) => {
  if (value.length > 0) {
    // return (<ion-icon p-float="right" name="checkmark-circle"></ion-icon>);
  }
};
 
export default class Register extends Component {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePass = this.onChangePass.bind(this);
    this.onChangeConfirmPass = this.onChangeConfirmPass.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.onToggleAgree = this.onToggleAgree.bind(this);
    this.onToggleShowPass = this.onToggleShowPass.bind(this);

    this.state = {
      email: "",
      password: "",
      confirmpass: "",
      loading: false,
      successful: false,
      message: "",
      showModal: false,
      didAgree: false,
      showpassword: false,
      showconfirmpass: false
    };

    this.nextPath = "/register/verify-code";
  }

  handleShow(e) {
    e.preventDefault();
    this.setState({ showModal: true });
  }

  handleClose() {
    this.setState({ showModal: false });
  }

  onToggleAgree() {
    this.setState({ didAgree: !this.state.didAgree, showModal: false });
  }

  handleRegister(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true,
      successful: false
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      const { email, password } = this.state;
      const nextPath = this.nextPath;
      AuthService.register({ email, password, nextPath }).then(
        response => {
          this.setState({
            message: "",
            successful: true
          });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            successful: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({ loading: false });
    }
  }

  onChangeEmail(e) {
    this.setState({ email: e.target.value });
  }

  onChangePass(e) {
    this.setState({ password: e.target.value });
  }

  onChangeConfirmPass(e) {
    this.setState({ confirmpass: e.target.value });
  }

  onToggleShowPass(target_name, e) {
    const name = `show${target_name}`;
    const value = !this.state[name];
    this.setState({ [name]: value });

    if (name === 'showpassword') {
      this.inputPass.focus();
    }
    else {
      this.inputConfirmPass.focus();
    }
  }

  componentDidMount() {
    this.timeout = setTimeout(() => {
      this.inputEmail && this.inputEmail.focus();
    }, 50);
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  render() {
    if (this.state.successful) {
      // form submission succeed
      return <Redirect to={this.nextPath} />;
    }
    else {
      const user = AuthService.getCurrentUser();
      if (user) {
        if (user.scope == "registration") {
          // user is still on registration process.
          const nextPath = user.nextPath || this.nextPath;
          return <Redirect to={nextPath} />;
        }
        else if (user.scope != "registration") {
          // user has logged in.
          const { from } = this.props.location.state || { from: { pathname: '/' } };
          return <Redirect to={from} />;
        }
      }
    }

    return (
      <div className="early-step register-page row">
        <div className="inner">
          <div className="logo-col"></div>
        </div>
        <div className="inner">
          <div className="max-width-330">
            <div className="logo-gif"></div>
            <h5 className="bold">Create New Account</h5>

            <Form
              className="mt-4 px-4"
              onSubmit={this.handleRegister}
              ref={c => { this.form = c; }}
            >
              <div>
                  <div className="form-group pos-relative">
                    <ion-icon p-float="left" name="person-outline"></ion-icon>
                    <CustomInput
                      inputRef={c => { this.inputEmail = c; }}
                      className="form-control form-control-lg form-control-tpl1"
                      type="email" name="email" placeholder="Email address"
                      value={this.state.email} onChange={this.onChangeEmail}
                      validations={[required, email, vpassing]}
                    />
                  </div>
                  <div className="form-group pos-relative">
                    <ion-icon p-float="left" name="key"></ion-icon>
                    <ion-icon p-float="right" name={this.state.showpassword ? "eye" : "eye-off"} onClick={this.onToggleShowPass.bind(this, "password")}></ion-icon>
                    <CustomInput
                      inputRef={c => { this.inputPass = c; }}
                      className="form-control form-control-lg form-control-tpl1"
                      type={this.state.showpassword ? "text" : "password"} name="password" placeholder="Password"
                      value={this.state.password} onChange={this.onChangePass}
                      validations={[required, vpassword, vpassing]}
                    />
                  </div>

                  <div className="form-group pos-relative">
                    <ion-icon p-float="left" name="key"></ion-icon>
                    <ion-icon p-float="right" name={this.state.showconfirmpass ? "eye" : "eye-off"} onClick={this.onToggleShowPass.bind(this, "confirmpass")}></ion-icon>
                    <CustomInput
                      inputRef={c => { this.inputConfirmPass = c; }}
                      className="form-control form-control-lg form-control-tpl1"
                      type={this.state.showconfirmpass ? "text" : "password"} name="confirmpass" placeholder="Confirm Password"
                      value={this.state.confirmpass} onChange={this.onChangeConfirmPass}
                      validations={[required, vconfirmpass, vpassing]}
                    />
                  </div>

                  <div className="form-group pos-relative">
                    <BsCheckbox id="check-agree" value={this.state.didAgree ? 1 : 0} checked={this.state.didAgree} validations={[required]}>
                      I have read and fully understood the
                      <Link to="" onClick={this.handleShow}> Terms of Use and Privacy Policy. </Link>
                    </BsCheckbox>
                  </div>

                  <div className="form-group">
                    <button
                      className="btn btn-lg btn-outline-primary btn-block"
                      disabled={this.state.loading}
                    >
                      {this.state.loading && (
                      <span className="submit-spinner spinner-border spinner-border-sm"></span>
                      )}
                      <span>REGISTER</span>
                    </button>
                 </div>
                </div>

              {this.state.message && (
                <div className="form-group">
                  <div
                    className={
                      this.state.successful
                        ? "alert alert-success"
                        : "alert alert-danger"
                    }
                    role="alert"
                  >
                    {this.state.message}
                  </div>
                </div>
              )}
              <CheckButton
                style={{ display: "none" }}
                ref={c => { this.checkBtn = c; }}
              />
            </Form>
          </div>
          <Modal className="terms-conditions-modal" show={this.state.showModal} onHide={this.handleClose} animation={false}>
            <Modal.Header>
              <Modal.Title className="h6">EVOR APP TERMS & CONDITIONS</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <h6>SITE MANAGEMENT</h6>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse auctor nisi in ligula faucibus feugiat nec quis mauris. Nam pharetra massa eu tincidunt viverra. Aliquam sagittis efficitur sem, et imperdiet tellus scelerisque in. Morbi eget nulla eu massa cursus vehicula non quis felis. Quisque quis dapibus massa. Quisque suscipit pulvinar ultrices. Fusce euismod mauris risus, consequat gravida augue vestibulum in. Praesent sit amet massa sit amet neque molestie pellentesque. Fusce commodo nulla nunc, ut fermentum massa laoreet at.</p>
              <h6>PRIVACY POLICY</h6>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse auctor nisi in ligula faucibus feugiat nec quis mauris. Nam pharetra massa eu tincidunt viverra. Aliquam sagittis efficitur sem, et imperdiet tellus scelerisque in. Morbi eget nulla eu massa cursus vehicula non quis felis. Quisque quis dapibus massa. Quisque suscipit pulvinar ultrices. Fusce euismod mauris risus, consequat gravida augue vestibulum in. Praesent sit amet massa sit amet neque molestie pellentesque. Fusce commodo nulla nunc, ut fermentum massa laoreet at.</p>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="primary" onClick={this.onToggleAgree}>{!this.state.didAgree ? 'AGREE' : 'DISAGREE'}</Button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    );
  }
}
