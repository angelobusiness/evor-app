import React, { Component } from "react";
import AuthService from "../services/auth.service";

import "./dashboard.scss";
import HistoryList from '../components/history';

export default class HistoryViewPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: AuthService.getCurrentUser(),
      showSidebar: false
    };

    const { match: { params } } = this.props;
    this.params = params;
  }

  render() {
    const classNames = "history-view-container history-view-" + this.params.type
    return (
      <div className="row">
        <div className="dashboard-tab-container dashboard-container with-nav-header with-main-nav-tabs max-w-600 px-0">
          <div className={classNames}>
            {/* <HistoryList type={this.params.type} />  */}
            <HistoryList type="transfer" />
          </div>
        </div>
      </div>
    );
  }
}
