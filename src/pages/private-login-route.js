import React from 'react'
import { Route, Redirect } from "react-router-dom";

import AuthService from "../services/auth.service";

const PrivateLoginRoute = ({ component: Component, ...rest }) => (
  <Route {...rest}
    render={props => {
      const user = AuthService.getCurrentUser();
      return user && user.scope && user.scope == "login_reset_pass" ? (
        <Component currentUser={user} {...props} />
      ) : (
          <Redirect to={{ pathname: '/404'}} />
        )
    }}
  />
);

export default PrivateLoginRoute;
