import React from 'react'
import { Route, Redirect } from "react-router-dom";

import AuthService from "../services/auth.service";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest}
    render={props => {
      const user = AuthService.getCurrentUser();
      return user ? (
        !user.scope ? (
          <Component currentUser={user} {...props} />
        ) : (
            <Redirect to={{ pathname: '/404' }} />
          )
      ) : (
          <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        )
    }}
  />
);

export default PrivateRoute;
