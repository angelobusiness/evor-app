import React, { Component } from "react";
import AuthService from "../services/auth.service";

export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: AuthService.getCurrentUser()
    };
  }

  render() {
    const { currentUser } = this.state;

    return (
      <div className="start-page display-center">
        <div className="max-width-330 px-4">
          <h2>Profile page</h2>
          <p><b>Email: </b><span>{currentUser.email}</span></p>
        </div>
      </div>
    );
  }
}
