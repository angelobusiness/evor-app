import React from 'react'
import { Route, Redirect } from "react-router-dom";

import AuthService from "../services/auth.service";

const PrivateRegRoute = ({ component: Component, ...rest }) => (
  <Route {...rest}
    render={props => {
      const user = AuthService.getCurrentUser();
      return user && user.scope && user.scope == "registration" ? (
        <Component currentUser={user} {...props} />
      ) : (
          <Redirect to={{ pathname: '/404'}} />
        )
    }}
  />
);

export default PrivateRegRoute;
