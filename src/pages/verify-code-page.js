import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Form from "react-validation/build/form";
import CustomInput from "../components/react-validation/input";
import CheckButton from "react-validation/build/button";

import AuthService from "../services/auth.service";

const required = (value) => {
  if (!value) {
    return (<>
      <p className="text-danger">This field is required.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

export default class VerifyCode extends Component {
  constructor(props) {
    super(props);
    this.onChangeCode = this.onChangeCode.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      user: props.currentUser,
      code: "",
      loading: false,
      successful: false,
      message: ""
    };

    this.nextPath = "/register/select-country";
  }

  onChangeCode(e) {
    this.setState({ code: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true,
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      const { code } = this.state;
      const nextPath = this.nextPath;
      AuthService.verifyCode({ nextPath, code }).then(
        response => {
          this.setState({
            message: "",
            successful: true
          });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            successful: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({ loading: false });
    }
  }

  render() {
    if (this.state.successful) {
      return <Redirect to={this.nextPath} />;
    }
    else {
      const user = AuthService.getCurrentUser();
      if (user) {
        if (user.scope == "registration") {
          // user is still on registration process.
          const nextPath = user.nextPath || this.nextPath;
          const { pathname } = this.props.location;
          if (pathname !== nextPath)
            return <Redirect to={nextPath} />;
        }
        else if (user.scope != "registration") {
          // user has logged in.
          const { from } = this.props.location.state || { from: { pathname: '/' } };
          return <Redirect to={from} />;
        }
      }
    }

    return (
      <div className="early-step verify-code row">
        <div className="inner">
          <div className="logo-col"></div>
        </div>
        <div className="inner">
          <div className="max-width-330">

            <ion-icon name="mail-unread-outline"></ion-icon>

            <p>The verification code has been sent to your registered email address. Please enter the code below.</p>

            <Form
              className="mt-4 px-4"
              onSubmit={this.handleSubmit}
              ref={c => { this.form = c; }}
            >
              <div className="form-group pos-relative">
                <CustomInput
                  className="form-control form-control-lg form-control-tpl1"
                  type="number" name="code" placeholder="Verification Code"
                  value={this.state.code} onChange={this.onChangeCode}
                  validations={[required]}
                />
                <p className="pt-1">Didn't get a code? <b onClick={() => {}}>Resend Code</b></p>
              </div>

              <div className="form-group">
                <button
                  className="btn btn-lg btn-outline-primary btn-block"
                  disabled={this.state.loading}
                >
                  {this.state.loading && (
                    <span className="submit-spinner spinner-border spinner-border-sm"></span>
                  )}
                  <span>VERIFY</span>
                </button>
              </div>

              {this.state.message && (
                <div className="form-group">
                  <div className="alert alert-danger" role="alert">
                    {this.state.message}
                  </div>
                </div>
              )}
              <CheckButton
                style={{ display: "none" }}
                ref={c => { this.checkBtn = c; }}
              />
            </Form>
          </div>
        </div>
      </div>
    );
  }
}
