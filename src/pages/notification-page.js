import React, { Component } from "react";
import { Link } from 'react-router-dom'
import Nav from 'react-bootstrap/Nav'
import NotificationList from "../components/notification";

// sample data
const notifications = [
  { type: "Transfer", date: "Apr 29, 2020 at 3PM", message: "You have successfully transferred amount to Name Surname" },
  { type: "Received", date: "Apr 29, 2020 at 3PM", message: "You have successfully received amount from Name Surname" },
  { type: "[important]", message: "Evor app will have system maintenance on Date 00, 2020. from 9:00 AM to 3:00 PM. We sincerely apologize for the inconvenience this has caused you." },
  { type: "Exchange", date: "Apr 29, 2020 at 3PM", message: "You have successfully exchanged 10,000 PHP to Vietnam Dong (VND)" },
  { type: "Received", date: "Apr 29, 2020 at 3PM", message: "You have successfully transferred amount to name" },
  { type: "Exchange", date: "Apr 29, 2020 at 3PM", message: "You have successfully transferred amount to name" },
  { type: "Exchange", date: "Apr 29, 2020 at 3PM", message: "You have successfully transferred amount to name" },
  { type: "Exchange", date: "Apr 29, 2020 at 3PM", message: "You have successfully transferred amount to name" },
  { type: "Exchange", date: "Apr 29, 2020 at 3PM", message: "You have successfully transferred amount to name" },
  { type: "Exchange", date: "Apr 29, 2020 at 3PM", message: "You have successfully transferred amount to name" },
];

export default class NotificationPage extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="row">
        <div className="dashboard-container with-nav-header max-w-600 px-0">
          <Nav className="nav-header max-w-600">
            <Nav.Item>
              <Nav.Link as={Link} className="nav-back" to="/home">Back</Nav.Link>
            </Nav.Item>
            <Nav.Item className="flex-fill text-center">
              <span className="nav-link nav-title">NOTIFICATIONS</span>
            </Nav.Item>
            <Nav.Item></Nav.Item>
          </Nav>
          <NotificationList notifications={notifications} />
        </div>
      </div>
    );
  }
}
