import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Form from "react-validation/build/form";
import CustomInput from "../components/react-validation/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";
import Recaptcha from "react-google-recaptcha";
import { config } from './../services/config';

import AuthService from "../services/auth.service";

const required = (value) => {
  if (!value) {
    return (<>
      <p className="text-danger">This field is required.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

const email = value => {
  if (!isEmail(value)) {
    return (<>
      <p className="text-danger">Invalid email.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

const vpassing = (value) => {
  if (value.length > 0) {
    // return (<ion-icon p-float="right" name="checkmark-circle"></ion-icon>);
  }
};

export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangeRecaptcha = this.onChangeRecaptcha.bind(this);
    this.onExpired = this.onExpired.bind(this);

    this.state = {
      email: "",
      recaptcha_response: "",
      loading: false,
      message: ""
    };

    this.nextPath = "/login/set-new-password";
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value
    });
  }

  onChangeRecaptcha(value) {
    this.setState({
      recaptcha_response: value
    });
  }

  onExpired() {
    this.setState({
      recaptcha_response: ""
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({
      message: "",
      loading: true
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      if (!this.state.recaptcha_response) {
        this.setState({
          message: "Unable to get reCaptcha response",
          loading: false
        });
        return;
      }

      const { email, recaptcha_response } = this.state;
      const nextPath = this.nextPath;
      AuthService.verifyEmailCaptcha({ nextPath, email, recaptcha_response }).then(
        () => {
          this.setState({
            message: "",
            successful: true
          });
          localStorage.setItem('codeGenAt', (new Date).getTime());
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({
        loading: false
      });
    }
  }

  render() {
    if (this.state.successful) {
      // form submission succeed
      return <Redirect to={this.nextPath} />;
    }
    else {
      const user = AuthService.getCurrentUser();
      if (user) {
        if (user.scope == "login_reset_pass") {
          // user is still on (login change password) process.
          const nextPath = user.nextPath || this.nextPath;
          const { pathname } = this.props.location;
          if (pathname !== nextPath)
            return <Redirect to={nextPath} />;
        }
        else if (user.scope != "registration") {
          // user has logged in.
          const { from } = this.props.location.state || { from: { pathname: '/' } };
          return <Redirect to={from} />;
        }
      }
    }

    return (
      <div className="early-step forgot-password-page row">
        <div className="inner">
          <div className="logo-col"></div>
        </div>
        <div className="inner">
          <div className="max-width-330">
            <h5 className="bold">Forgot Password</h5>
          </div>

          <Form
            className="mt-4"
            onSubmit={this.handleSubmit}
            ref={c => { this.form = c; }}
          >
            <div className="max-width-330 px-4">
              <div className="form-group pos-relative">
                <ion-icon p-float="left" name="person-outline"></ion-icon>
                <CustomInput
                  inputRef={c => { this.inputEmail = c; }}
                  className="form-control form-control-lg form-control-tpl1"
                  type="email" name="email" placeholder="Email address"
                  value={this.state.email} onChange={this.onChangeEmail}
                  validations={[required, email, vpassing]}
                />
              </div>
            </div>

            <div className="form-group pos-relative">
              <div className="recaptcha-wrapper">
                <p>Before you proceed to the next page, please complete the captcha below.</p>
                <Recaptcha
                  sitekey={config.RECAPTCHA_SITE_KEY}
                  onChange={this.onChangeRecaptcha}
                  onExpired={this.onExpired}
                />
              </div>
            </div>

            <div className="max-width-330 px-4">
              <div className="form-group">
                <button
                  className="btn btn-lg btn-outline-primary btn-block"
                  disabled={this.state.loading}
                >
                  {this.state.loading && (
                    <span className="submit-spinner submit-spinner spinner-border spinner-border-sm"></span>
                  )}
                  <span>NEXT</span>
                </button>
              </div>
            </div>

            {this.state.message && (
              <div className="max-width-330 px-4">
                <div className="form-group ">
                  <div className="alert alert-danger" role="alert">
                    {this.state.message}
                  </div>
                </div>
              </div>
            )}
            <CheckButton
              style={{ display: "none" }}
              ref={c => { this.checkBtn = c; }}
            />
          </Form>
        </div>
      </div>
    );
  }
}
