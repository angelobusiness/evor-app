import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Form from "react-validation/build/form";
import CheckButton from "react-validation/build/button";
import BsCountryDropdown from "../components/react-validation/bs-country-dropdown";
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

import AuthService from "../services/auth.service";

const required = (value) => {
  if (!value) {
    return (<>
      <p className="text-danger">This field is required.</p>
      <ion-icon p-float="right" name="alert-circle"></ion-icon>
    </>);
  }
};

export default class VerifyCode extends Component {
  constructor(props) {
    super(props);
    this.onSelectCountry = this.onSelectCountry.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onRegistrationComplete = this.onRegistrationComplete.bind(this);

    this.state = {
      user: props.currentUser,
      country: "",
      loading: false,
      toLogin: false,
      successful: false,
      message: ""
    };

    this.nextPath = "/login";
  }

  onRegistrationComplete() {
    // Remove user data with scope=registration
    AuthService.logout();
    // Redirect to login
    this.setState({ toLogin: true });
  }

  onSelectCountry(iso) {
    this.setState({ country: iso });
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log('Submitting');

    this.setState({
      message: "",
      loading: true,
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      const { country } = this.state;
      AuthService.setCountry({ country }).then(
        response => {
          this.setState({
            loading: false,
            message: "",
            successful: true
          });
        },
        error => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          this.setState({
            loading: false,
            successful: false,
            message: resMessage
          });
        }
      );
    } else {
      this.setState({ loading: false });
    }
  }

  render() {
    let showModal = false;
    if (this.state.toLogin) {
      return <Redirect to={this.nextPath} />;
    }
    else if (this.state.successful) {
      showModal = true;
    }
    else {
      const user = AuthService.getCurrentUser();
      if (user) {
        if (user.scope == "registration") {
          // user is still on registration process.
          if (user.status === "completed") {
            showModal = true;
          }
          else {
            const nextPath = user.nextPath || this.nextPath;
            const { pathname } = this.props.location;
            if (pathname !== nextPath)
              return <Redirect to={nextPath} />;
          }
        }
        else if (user.scope != "registration") {
          // user has logged in.
          const { from } = this.props.location.state || { from: { pathname: '/' } };
          return <Redirect to={from} />;
        }
      }
    }

    return (
      <div className="early-step verify-code row">
        <div className="inner">
          <div className="max-width-330">

            {showModal ? (
              <Modal className="registration-completed-modal" show={true} animation={false} centered>
                <Modal.Body>
                  <ion-icon name="checkmark-circle" size="large"></ion-icon>
                  <p className="modal-body-head mt-2">REGISTRATION COMPLETED</p>
                  <p><b>Welcome to EVOR!</b></p>
                  <p>
                    You have successfully created a new EVOR account.
                    Enjoy the benefits of this platform for your financial needs.
                    Please proceed to login page.
                  </p>
                  <p><b>Thank you!</b></p>
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="primary" onClick={this.onRegistrationComplete} style={{ letterSpacing: "1.2px", minWidth: "130px"}}>LOG IN</Button>
                </Modal.Footer>
              </Modal>
            ) : (
              <Form
                className="mt-5 px-4"
                onSubmit={this.handleSubmit}
                ref={c => { this.form = c; }}
              >
                <div className="form-group pos-relative">
                  <BsCountryDropdown onSelect={this.onSelectCountry} value={this.state.country} validations={[required]} />
                </div>

                <div className="form-group">
                  <ion-icon name="alert-circle" size="large"></ion-icon>
                  <p style={{ fontSize: "90%" }}>
                    This information cannot be changed in the future.
                    For legal compliance and government KYC requirements,
                    users will be required to submit their updated government
                    issued ID for authentication and verification.
                </p>
                </div>

                <div className="form-group">
                  <button
                    className="btn btn-lg btn-outline-primary btn-block"
                    disabled={this.state.loading || !this.state.country}
                  >
                    {this.state.loading && (
                      <span className="submit-spinner spinner-border spinner-border-sm"></span>
                    )}
                    <span>CONTINUE</span>
                  </button>
                </div>

                {this.state.message && (
                  <div className="form-group">
                    <div className="alert alert-danger" role="alert">
                      {this.state.message}
                    </div>
                  </div>
                )}
                <CheckButton
                  style={{ display: "none" }}
                  ref={c => { this.checkBtn = c; }}
                />
              </Form>
            )}

          </div>
        </div>
      </div>
    );
  }
}
