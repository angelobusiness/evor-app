import React, { Component } from "react";
import { Link } from 'react-router-dom'
import AuthService from "../services/auth.service";

import "./dashboard.scss";
import Tab from 'react-bootstrap/Tab';
import Nav from 'react-bootstrap/Nav';
import Sidebar from '../components/sidebar';

import CurrencyCard from "../components/currency-card";
const cards = {
  "PH": {
    "currency-name": "Philippines Peso",
    "balance": ["99,999", "00"],
    "code": "PHP"
  },
  "JP": {
    "currency-name": "Japanese Yen",
    "balance": ["99,999", "00"],
    "code": "JPY"
  },
  "SA": {
    "currency-name": "Saudi Riyal",
    "balance": ["99,999", "00"],
    "code": "SAR"
  },
  "KR": {
    "currency-name": "Korean Won",
    "balance": ["99,999", "00"],
    "code": "KRW"
  },
  "TH": {
    "currency-name": "Thai Bhat",
    "balance": ["99,999", "00"],
    "code": "THB"
  },
  "CN": {
    "currency-name": "Chinese Yen",
    "balance": ["99,999", "00"],
    "code": "CNY"
  },
  "VN": {
    "currency-name": "Vietnam Dong",
    "balance": ["99,999", "00"],
    "code": "VND"
  },
  "IN": {
    "currency-name": "Indian Rupee",
    "balance": ["99,999", "00"],
    "code": "INR"
  }
};
const cardIsos = Object.keys(cards);


export default class DashboardPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: AuthService.getCurrentUser(),
      showSidebar: false
    };
  }

  render() {
    return (
      <div className="row">
        <div className="dashboard-tab-container dashboard-container with-nav-header with-main-nav-tabs max-w-600 px-0">
          <Sidebar isOpen={this.state.showSidebar} onOverlayClick={() => { this.setState({ showSidebar: false }); }} />
          <Nav className="nav-header max-w-600">
            <Nav.Item>
              <Nav.Link className="nav-toggle-menu" onClick={(e) => { e.preventDefault(); this.setState({ showSidebar: true }); }}>Menu</Nav.Link>
            </Nav.Item>
            <Nav.Item className="flex-fill text-center">
              <span className="nav-link nav-title">BALANCE</span>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link as={Link} className="nav-notification" to="/notifications">Noti.</Nav.Link>
            </Nav.Item>
          </Nav>
          <Tab.Container id="dashboard-tab" defaultActiveKey="balance">
            <Tab.Content>
              <Tab.Pane eventKey="balance">
                {cardIsos.map((iso, index) => (
                  <CurrencyCard key={`${iso}-` + index.toString()} data={{ ...cards[iso], iso }} />
                ))}
              </Tab.Pane>
              <Tab.Pane eventKey="transfer">
                <p>Transfer</p>
              </Tab.Pane>
              <Tab.Pane eventKey="exchange">
                <p>Exchange</p>
              </Tab.Pane>
              <Tab.Pane eventKey="deposit">
                <p>Deposit</p>
              </Tab.Pane>
              <Tab.Pane eventKey="withdraw">
                <p>Withdraw</p>
              </Tab.Pane>
            </Tab.Content>
            <Nav fill variant="tabs" className="main-nav-tabs max-w-600">
              <Nav.Item>
                <Nav.Link className="tab-nav-link" eventKey="balance"><span className="temporary-icon"></span><span>Balance</span></Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link className="tab-nav-link" eventKey="transfer"><span className="temporary-icon"></span><span>Transfer</span></Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link className="tab-nav-link" eventKey="exchange"><span className="temporary-icon"></span><span>Exchange</span></Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link className="tab-nav-link" eventKey="deposit"><span className="temporary-icon"></span><span>Deposit</span></Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link className="tab-nav-link" eventKey="withdraw"><span className="temporary-icon"></span><span>Withdraw</span></Nav.Link>
              </Nav.Item>
            </Nav>
          </Tab.Container>
        </div>
      </div>
    );
  }
}
