import React, { Component } from "react";
import { Link } from "react-router-dom";
import globe from '../img/globe-vec.svg';

export default class StartPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  render() {
    return (
      <div className="start-page display-center">
        <div className="max-width-330 px-4">
          <img
            src={globe}
            className="img-globe"
            alt=""
          />
          <Link to={"/register"} className="btn btn-lg btn-outline-primary btn-block mt-5">Register Here!</Link>
          <Link to={"/login"} className="btn btn-lg btn-outline-primary btn-block mt-3">Login Here!</Link>
        </div>
      </div>
    );
  }
}
